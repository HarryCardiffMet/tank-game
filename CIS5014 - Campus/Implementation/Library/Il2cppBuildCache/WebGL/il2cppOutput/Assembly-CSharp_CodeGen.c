﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void MenuSceneLoader::Awake()
extern void MenuSceneLoader_Awake_m18B4E09AAFDC9578469D066C50A8AC0054AA34AC (void);
// 0x00000002 System.Void MenuSceneLoader::.ctor()
extern void MenuSceneLoader__ctor_m27FD45CA6C6C8B579D2FA4EEDABBA35F2C7EF6BC (void);
// 0x00000003 System.Void PauseMenu::Awake()
extern void PauseMenu_Awake_mB04BCBDA84AEC654D1976EC3DF61A0CF68D2C86C (void);
// 0x00000004 System.Void PauseMenu::MenuOn()
extern void PauseMenu_MenuOn_m8973DC956DD5235381EE4ACB4A182AA5BF0EF2EA (void);
// 0x00000005 System.Void PauseMenu::MenuOff()
extern void PauseMenu_MenuOff_mCD1AF41F916B0C02D2FD6F82377DBEAFF7C30720 (void);
// 0x00000006 System.Void PauseMenu::OnMenuStatusChange()
extern void PauseMenu_OnMenuStatusChange_mD1E8BD9B9CCB274A83FFF0FE2E06E6ABD0361B4A (void);
// 0x00000007 System.Void PauseMenu::Update()
extern void PauseMenu_Update_m191CABDC11442A2CC104FC8B3244D04826E7BD57 (void);
// 0x00000008 System.Void PauseMenu::.ctor()
extern void PauseMenu__ctor_mA1A281F3359C234E5CF24FFEAC20C12C48D69018 (void);
// 0x00000009 System.Void SceneAndURLLoader::Awake()
extern void SceneAndURLLoader_Awake_m8F276157A2A5FA943EF7918D6CCDB81273317E23 (void);
// 0x0000000A System.Void SceneAndURLLoader::SceneLoad(System.String)
extern void SceneAndURLLoader_SceneLoad_m2B09BD48F419F49A6BD461DBC7B2290EC8632B06 (void);
// 0x0000000B System.Void SceneAndURLLoader::LoadURL(System.String)
extern void SceneAndURLLoader_LoadURL_m47E3E286E80F2D5B3E6A164C32F7E1B473532AE2 (void);
// 0x0000000C System.Void SceneAndURLLoader::.ctor()
extern void SceneAndURLLoader__ctor_m6DEE574FADF9E3E894594690CB2755F69D5D4BE5 (void);
// 0x0000000D System.Void CameraSwitch::OnEnable()
extern void CameraSwitch_OnEnable_mD422991EDD9D880928644EE1BC4E557EE644679C (void);
// 0x0000000E System.Void CameraSwitch::NextCamera()
extern void CameraSwitch_NextCamera_m38AB4521C129032FA1DA5154E09D36D0FE2DB257 (void);
// 0x0000000F System.Void CameraSwitch::.ctor()
extern void CameraSwitch__ctor_m550FCA9B0C24BBBC8BDBCAAAAA7BBF26312399FE (void);
// 0x00000010 System.Void LevelReset::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void LevelReset_OnPointerClick_m9625C8235343CB1DE6B089AD6F2D5ACF738072A4 (void);
// 0x00000011 System.Void LevelReset::.ctor()
extern void LevelReset__ctor_mFD2BB9AF1A9D7E795DDAAE2F33C1F0EB1FB31F07 (void);
// 0x00000012 System.Void SmoothFollow::Start()
extern void SmoothFollow_Start_mA18519E2373B9E301AAD2E303727CF7389ACEED5 (void);
// 0x00000013 System.Void SmoothFollow::Update()
extern void SmoothFollow_Update_m77E28DF8A3F5CDA015FA08D613B6DF60C483936E (void);
// 0x00000014 System.Void SmoothFollow::.ctor()
extern void SmoothFollow__ctor_m6BA4AC412B8D5D63B87CCDAB25EDF2C446DD410B (void);
// 0x00000015 System.Void Controller::Start()
extern void Controller_Start_mD256D29928E12349C2565BD046108EE5A3537157 (void);
// 0x00000016 System.Void Controller::Update()
extern void Controller_Update_mBC27223743238412D4F6FF7B8B3E3FB2DF47F1A0 (void);
// 0x00000017 System.Void Controller::OnCollisionEnter(UnityEngine.Collision)
extern void Controller_OnCollisionEnter_mCE66864957827AFF8CB20FB4583059820B8BDD43 (void);
// 0x00000018 System.Void Controller::.ctor()
extern void Controller__ctor_m50D06C2CAE9B5496780A83ABCF68C529A44EE9F7 (void);
// 0x00000019 System.Void EnemyAi::Awake()
extern void EnemyAi_Awake_mC72F9A40EBE411F1941FCB8306ABF6B0FDD625E0 (void);
// 0x0000001A System.Void EnemyAi::Update()
extern void EnemyAi_Update_mABD0E26E476228647D3466A2FAA75A441E26F290 (void);
// 0x0000001B System.Void EnemyAi::Patroling()
extern void EnemyAi_Patroling_m883967EA61C42FA87B774DE2D30AFA47B69A2F7B (void);
// 0x0000001C System.Void EnemyAi::SearchWalkPoint()
extern void EnemyAi_SearchWalkPoint_m76469F334DFCEE54D6DD8D088394B250510302F6 (void);
// 0x0000001D System.Void EnemyAi::ChasePlayer()
extern void EnemyAi_ChasePlayer_m8A6249645B99207EAD5C0A5B78B77C6ED96A7BC7 (void);
// 0x0000001E System.Void EnemyAi::AttackPlayer()
extern void EnemyAi_AttackPlayer_m4990F8CEE4885F8FB762EE3B9629D803BA0324A8 (void);
// 0x0000001F System.Void EnemyAi::ResetAttack()
extern void EnemyAi_ResetAttack_m9D2FBF1046ED7FFE8E3E571F388BC47AC7391695 (void);
// 0x00000020 System.Void EnemyAi::TakeDamage(System.Int32)
extern void EnemyAi_TakeDamage_mE290C467B41A5637015EE042C1254008A26E4784 (void);
// 0x00000021 System.Void EnemyAi::DestroyEnemy()
extern void EnemyAi_DestroyEnemy_mB2EF46BB660F9AAB688F81936A77992527A0B5DB (void);
// 0x00000022 System.Void EnemyAi::OnDrawGizmosSelected()
extern void EnemyAi_OnDrawGizmosSelected_mC2730783D93FF42B47F07830F798473BDE77D51D (void);
// 0x00000023 System.Void EnemyAi::.ctor()
extern void EnemyAi__ctor_m194E3162B20373C86DE944DC595B8000BAA30DFE (void);
// 0x00000024 System.Void FuelScript::Start()
extern void FuelScript_Start_m4AAF88A3BFF0C0E30D15BCB0F76862848150B191 (void);
// 0x00000025 System.Void FuelScript::Update()
extern void FuelScript_Update_m2418D8EBA26F8BDEC738304716E6FFD9CD010B7B (void);
// 0x00000026 System.Void FuelScript::OnTriggerEnter(UnityEngine.Collider)
extern void FuelScript_OnTriggerEnter_m0297BB10FDDC30FABA7095E8A270778554DDC5BC (void);
// 0x00000027 System.Void FuelScript::.ctor()
extern void FuelScript__ctor_m35E18F4CC51CAC583B65EFF7A788D35815EED8B8 (void);
// 0x00000028 System.Void MouseLooker::Start()
extern void MouseLooker_Start_m62C65D9E72654C73653B641DE577A0ED93A3D437 (void);
// 0x00000029 System.Void MouseLooker::Update()
extern void MouseLooker_Update_m9B2049DFCDBC73BA481BA00988231B1011C28E55 (void);
// 0x0000002A System.Void MouseLooker::LockCursor(System.Boolean)
extern void MouseLooker_LockCursor_mB30F16B0D059038F92963F3A9916213DC59A1E70 (void);
// 0x0000002B System.Void MouseLooker::LookRotation()
extern void MouseLooker_LookRotation_mDBD54C8212E2489D0C18C35F277ABE6CDDDC171B (void);
// 0x0000002C UnityEngine.Quaternion MouseLooker::ClampRotationAroundXAxis(UnityEngine.Quaternion)
extern void MouseLooker_ClampRotationAroundXAxis_mCF88B2CD1B917D1E9EFB8275F6D14D63C67D7C56 (void);
// 0x0000002D System.Void MouseLooker::.ctor()
extern void MouseLooker__ctor_m57240C9A9D42D49929076F9A34D4CD8A4E8C9B98 (void);
// 0x0000002E System.Void MouseLookerV2::Start()
extern void MouseLookerV2_Start_mA11A4A7D71BAFDCAEE74460F0BD4866F4A90D7E1 (void);
// 0x0000002F System.Void MouseLookerV2::Update()
extern void MouseLookerV2_Update_m259C9B1E8ABA111A7ACD5BEB60E92CC5C41AAAF8 (void);
// 0x00000030 System.Void MouseLookerV2::LockCursor(System.Boolean)
extern void MouseLookerV2_LockCursor_mFF57D1304993DA6050820DB13A1EC192F696A07E (void);
// 0x00000031 System.Void MouseLookerV2::LookRotation()
extern void MouseLookerV2_LookRotation_mBF2455CF5BFB2F27E8C50D5078220837FA91DF97 (void);
// 0x00000032 UnityEngine.Quaternion MouseLookerV2::ClampRotationAroundXAxis(UnityEngine.Quaternion)
extern void MouseLookerV2_ClampRotationAroundXAxis_m99DE6F4710FE19AF22B2A346884E817E890603AA (void);
// 0x00000033 System.Void MouseLookerV2::.ctor()
extern void MouseLookerV2__ctor_m48AC9A5B355C2ADFC7CAC9C34C856F27F5728727 (void);
// 0x00000034 System.Void PlayerScript::Start()
extern void PlayerScript_Start_m23E83EF89671F830F89218AF65D8C573AF6D8DC5 (void);
// 0x00000035 System.Void PlayerScript::Update()
extern void PlayerScript_Update_m4B7EDA0BE2C7C0C1ED25E80E6D341E17421FD3D2 (void);
// 0x00000036 System.Void PlayerScript::OnGUI()
extern void PlayerScript_OnGUI_m0AE0D90E5422FDF8E7D021EA5BCA8C81B47314A2 (void);
// 0x00000037 System.Void PlayerScript::.ctor()
extern void PlayerScript__ctor_m8FC59B4E46D210A2C41D8B1F546C2603EC786E5D (void);
// 0x00000038 System.Void SceneChanger::ChangeScene(System.String)
extern void SceneChanger_ChangeScene_m00182E4E31B32E2E0E818E34FEEF207B6CF9C790 (void);
// 0x00000039 System.Void SceneChanger::Exit()
extern void SceneChanger_Exit_m7367524BED2B13B8A53A9698F3AA3A9B2D31CCD3 (void);
// 0x0000003A System.Void SceneChanger::.ctor()
extern void SceneChanger__ctor_m11AE9A596EFE92EE1AA22BD7A48AB0C1D758AB1D (void);
// 0x0000003B System.Void CameraFollow::Start()
extern void CameraFollow_Start_mC13DAB135378AADB3E72275F75CDEAF53343810D (void);
// 0x0000003C System.Void CameraFollow::LateUpdate()
extern void CameraFollow_LateUpdate_m140D0956A03E1CD6C6DF9B74330BDA2A0813248F (void);
// 0x0000003D System.Void CameraFollow::.ctor()
extern void CameraFollow__ctor_m29F88CCFD2ED12A7BCC75A9BBA892CEF179C83DE (void);
// 0x0000003E System.Void Win1::OnTriggerEnter(UnityEngine.Collider)
extern void Win1_OnTriggerEnter_m7680D033543EF62F4C7CC47ABA8FF5C2AE7A92FD (void);
// 0x0000003F System.Void Win1::.ctor()
extern void Win1__ctor_m1294F71D5CCF9115BC3C692D16B87F110335DC86 (void);
// 0x00000040 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Awake()
extern void ParticleSceneControls_Awake_m3268C5B5B81D4EE59C85C27AED5CA3956C284DA4 (void);
// 0x00000041 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::OnDisable()
extern void ParticleSceneControls_OnDisable_mEB5EAF7BC1928EBEFD83E651459327948255D811 (void);
// 0x00000042 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Previous()
extern void ParticleSceneControls_Previous_mC2FE564898A6019238C5611E63B5EC3581CEAA63 (void);
// 0x00000043 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Next()
extern void ParticleSceneControls_Next_m4AB6C18CBE0148C4D354FDCAD5EE45D472577B8E (void);
// 0x00000044 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Update()
extern void ParticleSceneControls_Update_m7B5894AE70D3733C901BFFF529195AC1467D632B (void);
// 0x00000045 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::KeyboardInput()
extern void ParticleSceneControls_KeyboardInput_m29C494EC6ACF4EE139686EF47C3A167D1007970F (void);
// 0x00000046 System.Boolean UnityStandardAssets.SceneUtils.ParticleSceneControls::CheckForGuiCollision()
extern void ParticleSceneControls_CheckForGuiCollision_mB8EDC5E34CE1B59986991852009A011D9514A8FB (void);
// 0x00000047 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::Select(System.Int32)
extern void ParticleSceneControls_Select_m123BA8121C7C5C454EF649A13C724F2622B4517A (void);
// 0x00000048 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::.ctor()
extern void ParticleSceneControls__ctor_mFE76B488C636F213B77193EDFF38F514A3186631 (void);
// 0x00000049 System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls::.cctor()
extern void ParticleSceneControls__cctor_mBA868BA9BC2400AD1AB420B3D0AB42D863358338 (void);
// 0x0000004A System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystem::.ctor()
extern void DemoParticleSystem__ctor_mA046B73C934441755CA5D57991D7C453645AD8CE (void);
// 0x0000004B System.Void UnityStandardAssets.SceneUtils.ParticleSceneControls/DemoParticleSystemList::.ctor()
extern void DemoParticleSystemList__ctor_mAFBB16272EB2E695826211DD030553401DF6E83B (void);
// 0x0000004C System.Void UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::Update()
extern void PlaceTargetWithMouse_Update_m28E68FE68FA0185C0D20A6001F4C262868348BE3 (void);
// 0x0000004D System.Void UnityStandardAssets.SceneUtils.PlaceTargetWithMouse::.ctor()
extern void PlaceTargetWithMouse__ctor_m3582FA7DC3CF8198244F9EB014FE12AC97985FE7 (void);
// 0x0000004E System.Void UnityStandardAssets.SceneUtils.SlowMoButton::Start()
extern void SlowMoButton_Start_m8F334286C4BEE772EAA4842F431E01BB4929A04C (void);
// 0x0000004F System.Void UnityStandardAssets.SceneUtils.SlowMoButton::OnDestroy()
extern void SlowMoButton_OnDestroy_mE8D51330ED641B12795E984B6F0ECCB00536DBDB (void);
// 0x00000050 System.Void UnityStandardAssets.SceneUtils.SlowMoButton::ChangeSpeed()
extern void SlowMoButton_ChangeSpeed_m0C19576C039AA7BBBC478C87A6C964376BA58EAB (void);
// 0x00000051 System.Void UnityStandardAssets.SceneUtils.SlowMoButton::.ctor()
extern void SlowMoButton__ctor_m993A85A43C7462445119B63636FD65A6F8C0F6BF (void);
static Il2CppMethodPointer s_methodPointers[81] = 
{
	MenuSceneLoader_Awake_m18B4E09AAFDC9578469D066C50A8AC0054AA34AC,
	MenuSceneLoader__ctor_m27FD45CA6C6C8B579D2FA4EEDABBA35F2C7EF6BC,
	PauseMenu_Awake_mB04BCBDA84AEC654D1976EC3DF61A0CF68D2C86C,
	PauseMenu_MenuOn_m8973DC956DD5235381EE4ACB4A182AA5BF0EF2EA,
	PauseMenu_MenuOff_mCD1AF41F916B0C02D2FD6F82377DBEAFF7C30720,
	PauseMenu_OnMenuStatusChange_mD1E8BD9B9CCB274A83FFF0FE2E06E6ABD0361B4A,
	PauseMenu_Update_m191CABDC11442A2CC104FC8B3244D04826E7BD57,
	PauseMenu__ctor_mA1A281F3359C234E5CF24FFEAC20C12C48D69018,
	SceneAndURLLoader_Awake_m8F276157A2A5FA943EF7918D6CCDB81273317E23,
	SceneAndURLLoader_SceneLoad_m2B09BD48F419F49A6BD461DBC7B2290EC8632B06,
	SceneAndURLLoader_LoadURL_m47E3E286E80F2D5B3E6A164C32F7E1B473532AE2,
	SceneAndURLLoader__ctor_m6DEE574FADF9E3E894594690CB2755F69D5D4BE5,
	CameraSwitch_OnEnable_mD422991EDD9D880928644EE1BC4E557EE644679C,
	CameraSwitch_NextCamera_m38AB4521C129032FA1DA5154E09D36D0FE2DB257,
	CameraSwitch__ctor_m550FCA9B0C24BBBC8BDBCAAAAA7BBF26312399FE,
	LevelReset_OnPointerClick_m9625C8235343CB1DE6B089AD6F2D5ACF738072A4,
	LevelReset__ctor_mFD2BB9AF1A9D7E795DDAAE2F33C1F0EB1FB31F07,
	SmoothFollow_Start_mA18519E2373B9E301AAD2E303727CF7389ACEED5,
	SmoothFollow_Update_m77E28DF8A3F5CDA015FA08D613B6DF60C483936E,
	SmoothFollow__ctor_m6BA4AC412B8D5D63B87CCDAB25EDF2C446DD410B,
	Controller_Start_mD256D29928E12349C2565BD046108EE5A3537157,
	Controller_Update_mBC27223743238412D4F6FF7B8B3E3FB2DF47F1A0,
	Controller_OnCollisionEnter_mCE66864957827AFF8CB20FB4583059820B8BDD43,
	Controller__ctor_m50D06C2CAE9B5496780A83ABCF68C529A44EE9F7,
	EnemyAi_Awake_mC72F9A40EBE411F1941FCB8306ABF6B0FDD625E0,
	EnemyAi_Update_mABD0E26E476228647D3466A2FAA75A441E26F290,
	EnemyAi_Patroling_m883967EA61C42FA87B774DE2D30AFA47B69A2F7B,
	EnemyAi_SearchWalkPoint_m76469F334DFCEE54D6DD8D088394B250510302F6,
	EnemyAi_ChasePlayer_m8A6249645B99207EAD5C0A5B78B77C6ED96A7BC7,
	EnemyAi_AttackPlayer_m4990F8CEE4885F8FB762EE3B9629D803BA0324A8,
	EnemyAi_ResetAttack_m9D2FBF1046ED7FFE8E3E571F388BC47AC7391695,
	EnemyAi_TakeDamage_mE290C467B41A5637015EE042C1254008A26E4784,
	EnemyAi_DestroyEnemy_mB2EF46BB660F9AAB688F81936A77992527A0B5DB,
	EnemyAi_OnDrawGizmosSelected_mC2730783D93FF42B47F07830F798473BDE77D51D,
	EnemyAi__ctor_m194E3162B20373C86DE944DC595B8000BAA30DFE,
	FuelScript_Start_m4AAF88A3BFF0C0E30D15BCB0F76862848150B191,
	FuelScript_Update_m2418D8EBA26F8BDEC738304716E6FFD9CD010B7B,
	FuelScript_OnTriggerEnter_m0297BB10FDDC30FABA7095E8A270778554DDC5BC,
	FuelScript__ctor_m35E18F4CC51CAC583B65EFF7A788D35815EED8B8,
	MouseLooker_Start_m62C65D9E72654C73653B641DE577A0ED93A3D437,
	MouseLooker_Update_m9B2049DFCDBC73BA481BA00988231B1011C28E55,
	MouseLooker_LockCursor_mB30F16B0D059038F92963F3A9916213DC59A1E70,
	MouseLooker_LookRotation_mDBD54C8212E2489D0C18C35F277ABE6CDDDC171B,
	MouseLooker_ClampRotationAroundXAxis_mCF88B2CD1B917D1E9EFB8275F6D14D63C67D7C56,
	MouseLooker__ctor_m57240C9A9D42D49929076F9A34D4CD8A4E8C9B98,
	MouseLookerV2_Start_mA11A4A7D71BAFDCAEE74460F0BD4866F4A90D7E1,
	MouseLookerV2_Update_m259C9B1E8ABA111A7ACD5BEB60E92CC5C41AAAF8,
	MouseLookerV2_LockCursor_mFF57D1304993DA6050820DB13A1EC192F696A07E,
	MouseLookerV2_LookRotation_mBF2455CF5BFB2F27E8C50D5078220837FA91DF97,
	MouseLookerV2_ClampRotationAroundXAxis_m99DE6F4710FE19AF22B2A346884E817E890603AA,
	MouseLookerV2__ctor_m48AC9A5B355C2ADFC7CAC9C34C856F27F5728727,
	PlayerScript_Start_m23E83EF89671F830F89218AF65D8C573AF6D8DC5,
	PlayerScript_Update_m4B7EDA0BE2C7C0C1ED25E80E6D341E17421FD3D2,
	PlayerScript_OnGUI_m0AE0D90E5422FDF8E7D021EA5BCA8C81B47314A2,
	PlayerScript__ctor_m8FC59B4E46D210A2C41D8B1F546C2603EC786E5D,
	SceneChanger_ChangeScene_m00182E4E31B32E2E0E818E34FEEF207B6CF9C790,
	SceneChanger_Exit_m7367524BED2B13B8A53A9698F3AA3A9B2D31CCD3,
	SceneChanger__ctor_m11AE9A596EFE92EE1AA22BD7A48AB0C1D758AB1D,
	CameraFollow_Start_mC13DAB135378AADB3E72275F75CDEAF53343810D,
	CameraFollow_LateUpdate_m140D0956A03E1CD6C6DF9B74330BDA2A0813248F,
	CameraFollow__ctor_m29F88CCFD2ED12A7BCC75A9BBA892CEF179C83DE,
	Win1_OnTriggerEnter_m7680D033543EF62F4C7CC47ABA8FF5C2AE7A92FD,
	Win1__ctor_m1294F71D5CCF9115BC3C692D16B87F110335DC86,
	ParticleSceneControls_Awake_m3268C5B5B81D4EE59C85C27AED5CA3956C284DA4,
	ParticleSceneControls_OnDisable_mEB5EAF7BC1928EBEFD83E651459327948255D811,
	ParticleSceneControls_Previous_mC2FE564898A6019238C5611E63B5EC3581CEAA63,
	ParticleSceneControls_Next_m4AB6C18CBE0148C4D354FDCAD5EE45D472577B8E,
	ParticleSceneControls_Update_m7B5894AE70D3733C901BFFF529195AC1467D632B,
	ParticleSceneControls_KeyboardInput_m29C494EC6ACF4EE139686EF47C3A167D1007970F,
	ParticleSceneControls_CheckForGuiCollision_mB8EDC5E34CE1B59986991852009A011D9514A8FB,
	ParticleSceneControls_Select_m123BA8121C7C5C454EF649A13C724F2622B4517A,
	ParticleSceneControls__ctor_mFE76B488C636F213B77193EDFF38F514A3186631,
	ParticleSceneControls__cctor_mBA868BA9BC2400AD1AB420B3D0AB42D863358338,
	DemoParticleSystem__ctor_mA046B73C934441755CA5D57991D7C453645AD8CE,
	DemoParticleSystemList__ctor_mAFBB16272EB2E695826211DD030553401DF6E83B,
	PlaceTargetWithMouse_Update_m28E68FE68FA0185C0D20A6001F4C262868348BE3,
	PlaceTargetWithMouse__ctor_m3582FA7DC3CF8198244F9EB014FE12AC97985FE7,
	SlowMoButton_Start_m8F334286C4BEE772EAA4842F431E01BB4929A04C,
	SlowMoButton_OnDestroy_mE8D51330ED641B12795E984B6F0ECCB00536DBDB,
	SlowMoButton_ChangeSpeed_m0C19576C039AA7BBBC478C87A6C964376BA58EAB,
	SlowMoButton__ctor_m993A85A43C7462445119B63636FD65A6F8C0F6BF,
};
static const int32_t s_InvokerIndices[81] = 
{
	1235,
	1235,
	1235,
	1235,
	1235,
	1235,
	1235,
	1235,
	1235,
	1048,
	1048,
	1235,
	1235,
	1235,
	1235,
	1048,
	1235,
	1235,
	1235,
	1235,
	1235,
	1235,
	1048,
	1235,
	1235,
	1235,
	1235,
	1235,
	1235,
	1235,
	1235,
	1038,
	1235,
	1235,
	1235,
	1235,
	1235,
	1048,
	1235,
	1235,
	1235,
	1065,
	1235,
	854,
	1235,
	1235,
	1235,
	1065,
	1235,
	854,
	1235,
	1235,
	1235,
	1235,
	1235,
	1048,
	1235,
	1235,
	1235,
	1235,
	1235,
	1048,
	1235,
	1235,
	1235,
	1235,
	1235,
	1235,
	1235,
	1222,
	1038,
	1235,
	2099,
	1235,
	1235,
	1235,
	1235,
	1235,
	1235,
	1235,
	1235,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	81,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
